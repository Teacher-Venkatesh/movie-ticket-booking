# Movie Ticket Booking 

## Overview 

We need to design an online movie ticket booking system where a user can search for a movie in a given city and book it. 

In order to do this, we have decomposed the entire movie ticket booking application into various epics. In each epic you will be working on various user stories within a given sprint. 

Currently, you are in sprint – 1 which is two weeks' timeline, and as part of your user story 1, you are going to design a signup widget. In this sprint, you will be working as a front-end developer. Your task details are given below. 

| Epic        | User Management                                          | Epic ID        | E-001     |
|-------------|----------------------------------------------------------|----------------|-----------|

| Sprint      | Sprint 1                                                 |
|-------------|----------------------------------------------------------|

| User Story  | Signup Widget                                            | User story ID  | US-001    |
|-------------|----------------------------------------------------------|----------------|-----------|

| Task        | Signup page                                              | Task ID        | FE-T-001  |
|-------------|----------------------------------------------------------|----------------|-----------|

| Role        | Frontend Developer                                       |
|-------------|----------------------------------------------------------|

| Tech Stack  | Angular 7+   Google Material Design   Bootstrap / Bulma  |
|-------------|----------------------------------------------------------|

## Key points to remember: 

- The id (for frontend) mentioned in the Task document should not be modified at any cost. Failing to do may fail test cases. 

- Remember to check the screenshots provided with the document. Strictly adhere to id mapping and attribute mapping. Failing to do may fail test cases. 

- Strictly adhere to the proper project scaffolding (Folder structure), coding conventions, method definitions and return types. 

- Adhere strictly to the endpoints given below. 

 
## Application assumptions: 

- The login page should be the first page rendered when the application loads. 

- Manual routing should be restricted by using AuthGaurd by implementing the canActivate interface. For example, if the user enters as http://localhost:4200/signup or http://localhost:4200/home the page should not navigate to the corresponding page instead it should redirect to the login page. 

- Unless logged into the system, the user cannot navigate to any other pages. 

- Logging out must again redirect to the login page. 

- To navigate to the admin side, you can store a user type as admin in the database with a username and password as admin. 

- Use admin/admin as the username and password to navigate to the admin dashboard. 

_**Iteration 1: Signup Component**_

Create a component called as signup with the following fields.  
```
Email 
Username 
Mobile Number 
Password 
Confirm Password
``` 

 
Note: Kindly refer the sample output screenshot given above. The ids mapped to each field should be kept as such.  

_**Iteration 2: Validations**_

Add proper validations to the fields mentioned in the signup page.  

_**Iteration 3: Signup Service**_ 

Create a service that maps the post request to the Signup component. Create a mock API called users.json to test your signup endpoints. You can use postman/swagger to test your API. 

| Signup  | /signup  | POST  | true/false  |
|---------|----------|-------|-------------|

 

_**Iteration 4: Adding Styles to Signup**_ 

Enhance the design using material design library or bootstrap. 

 

 
