# Movie Ticket Booking System 

## Overview: 

   Mr.Viewer is an online application for easily browse and play movies by genres, and language, etc. 

## Users of the System: 

1. Admin 
1. User 


## Functional Requirements:	 
```
Build an application that customer can access the movie. 

Admin should provide for automatic tagging of movie and categorise them. 

Admin should proper movie details such as language, artist, movie name etc.. 

User will give the like/dislike for the movie 

This application should have a provision to maintain a database for user information, movie portfolio 

Authenticity for adding users is utmost important for such a website.  

Definitely one should not be allowed to have more than one profile, validation of user should be done using email id. 

Categorize the movie based on genres. 

```

 

While the above ones are the basic functional features expected, the below ones can be nice to have add-on features: 

There are lot of freeware & open-source applications available for many social functions. The team is expected to search & leverage these to the maximum. 

Multi-factor authentication for the sign-in process 

 

Output/ Post Condition: 

Reports for users 

Reports for Site admit. The operational reports to make the site better in future 

Non-Functional Requirements: 

 

Security 

App Platform – UserName/Password-Based Credentials 

Sensitive data has to be categorized and stored in a secure manner 

Secure connection for transmission of any data 

Performance 

Peak Load Performance  

Mr.Viewer < 3 Sec 

Admin application < 2 Sec 

Non-Peak Load Performance 

 

Availability 

99.99 % Availability 

Standard Features 

Scalability 

Maintainability 

Usability 

Availability 

Failover 

Logging & Auditing 

The system should support logging(app/web/DB) & auditing at all levels 

Monitoring 

Should be able to monitor via as-is enterprise monitoring tools 

Cloud 

The Solution should be made Cloud-ready and should have a minimum impact when moving away to Cloud infrastructure 

Browser Compatible 

IE 7+ 

Mozilla Firefox Latest 

Google Chrome Latest 

Mobile Ready 

 

Technology Stack 

Front End 

Angular 7+  

Google Material Design  

Bootstrap / Bulma 

Server Side 

Spring Boot 

Spring Web (Rest Controller) 

Spring Security 

Spring AOP 

Spring Hibernate 

Core Platform 

OpenJDK 11 

Database 

MySQL 

 

 

Platform Pre-requisites (Do’s and Don’ts): 

The Angular app should run in port 8081. Do not run the Angular app in the port: 4200. 

Spring boot app should run in port 8080. 

 

Key points to remember: 

The id (for frontend) and attributes(backend) mentioned in the SRS should not be modified at any cost. Failing to do may fail test cases. 

Remember to check the screenshots provided with the SRS. Strictly adhere to id mapping and attribute mapping. Failing to do may fail test cases. 

Strictly adhere to the proper project scaffolding (Folder structure), coding conventions, method definitions and return types. 

Adhere strictly to the endpoints given below. 

 

Application assumptions: 

The login page should be the first page rendered when the application loads. 

Manual routing should be restricted by using AuthGaurd by implementing the canActivate interface. For example, if the user enters as http://localhost:4200/signup or http://localhost:4200/home the page should not navigate to the corresponding page instead it should redirect to the login page. 

Unless logged into the system, the user cannot navigate to any other pages. 

Logging out must again redirect to the login page. 

To navigate to the admin side, you can store a user type as admin in the database with a username and password as admin. 

Use admin/admin as the username and password to navigate to the admin dashboard. 

 

Validations: 

Basic email validation should be performed. 

Basic mobile validation should be performed. 

Project Tasks: 

API Endpoints: 

USER 

 	 	 
Action 

URL 

Method 

Response 

Login 

/login 

POST 

true/false 

Signup 

/signup 

POST 

true/false 

Get All Movie 

/movie 

GET 

Array of Movie 

Get Movie 

/movie/{id} 

GET 

Movie Details 

Add Likes 

/like/{id} 

POST 

Like added to Songs 

Remove Likes 

/like/{id} 

DELETE 

Like removed 

ADMIN 

 

 

 

Action 

URL 

Method 

Response 

Get All User 

/admin 

GET 

Array of Users 

Delete User 

/admin/delete/{id} 

DELETE 

User deleted 

User Edit 

/admin/userEdit/{id} 

PUT 

Save the Changes 

Get All Movie 

/admin/movie 

GET 

Array of Movie 

Delete Movie 

/admin/movie/{id} 

DELETE 

Movie deleted 

Update Movie 

/admin/movie/{id} 

PUT 

Save the Changes 

Get All Comment 

/admin/comment 

GET 

Array of Comments 

 

Frontend:  

User: 

Login: 

Output Screenshot: 

 

 

Signup: 

Output Screenshot: 

 

Home: 

Output Screenshot: 

 

 

Movie: 

Output Screenshot: 

 

 

Admin: 

Home: 

Output Screenshot: 

 

 

Movie: 

Output Screenshot: 

 

Backend: 

Class and Method description: 

 

Model Layer: 

 

UserModel: This class stores the user type (admin or user) and all user information. 

Attributes: 

email: String 

password: String 

username: String 

mobileNumber: String 

active: Boolean 

role: String 

Methods: - 

 

LoginModel: This class contains the email and password of the user. 

Attributes: 

email: String 

password: String 

Methods: - 

MovieModel: This class stores the details of the product. 

Attributes: 

movieId: String 

movieName: String 

movieUrl: String 

moviePosterUrl: String 

movieCast: List<String> 

like: LikeModel 

Methods: - 

LikeModel: This class stores the cart items. 

Attributes: 

Id: String 

noOfLike: int 

likedUser: List<UserModel> 

Methods: -  

Controller Layer: 

SignupController: This class control the user signup 

Attributes:  - 

Methods: 

saveUser(UserModel user): This method helps to store users in the database and return true or false based on the database transaction. 

LoginController: This class controls the user login. 

Attributes: - 

Methods:  

checkUser(LoginModel data): This method helps the user to sign up for the application and must return true or false 

UserController: This class controls the add/edit/update/view User. 

Attributes: - 

Methods:  

List<UserModel> getUser(): This method helps the admin to fetch all User from the database. 

List< UserModel > getOnlineUser(): This method helps to retrieve all the online user from the database. 

userModel userEditData(String id): This method helps to retrieve a user from the database based on the user id. 

userEditSave(UserModel data): This method helps to edit a user and save it to the database. 

userDelete (String id): This method helps to delete a user from the database. 

MovieController: This class helps in adding movie, deleting movie, updating. 

Attributes: - 

Methods: 

addMovie(MovieModel image): This method helps the user to add the movie to the database. 

List<MovieModel> getAllMovie(): This method helps the user to fetch all the movie from the database. 

MovieModel showMovie(String id): This method helps the user to play the movie.  

deleteMovie(String id): This method helps to delete a movie from the database. 

updateMovie(MovieModel data): This method helps to update a movie details from the database. 

CommentController: This class helps in adding, deleting, updating the comment. 

Attributes: - 

Methods: 

addLike(String Id): This method helps the user to add a Like for the selected song. 

removeLive (String id): This method helps to remove a Like from the song. 

getLikeCount(String id): This method will return the number of likes based on song id. 
